#!/usr/scripts/env python

from distutils.core import setup

NAME = "sd_cooling_rate"
VERSION = "1.0.0"

setup(name=NAME,
      version=VERSION,
      license="MIT",
      description="Utility to compute colling rates based on a single domain model",
      url="https://bitbucket.org/micromag{}}/src/master/".format(NAME),
      download_url="https://bitbucket.org/micromag/{}/get/{}-{}.zip".format(NAME, VERSION),
      keywords=["micromagnetics", "database"],
      install_requires=["pandas",
                        "matplotlib"
                        "PyYAML",
                        "Deprecated"],
      author="L. Nagy, W. Williams",
      author_email="lnagy2@ed.ac.uk",
      packages=["m4db_database"],
      package_dir={"m4db_database": "lib/m4db_database"},
      scripts=[
            "scripts/m4db_export_anisotropy_form",
            "scripts/m4db_export_database_from_neb",
            "scripts/m4db_export_db_user",
            "scripts/m4db_export_geometry",
            "scripts/m4db_export_model",
            "scripts/m4db_export_material",
            "scripts/m4db_export_neb",
            "scripts/m4db_export_neb_calculation_type",
            "scripts/m4db_export_physical_constant",
            "scripts/m4db_export_size_convention",
            "scripts/m4db_export_software",
            "scripts/m4db_export_unit",
            "scripts/m4db_import_anisotropy_form",
            "scripts/m4db_import_db_user",
            "scripts/m4db_import_geometry",
            "scripts/m4db_import_physical_constant",
            "scripts/m4db_import_size_convention",
            "scripts/m4db_import_software",
            "scripts/m4db_import_unit",
            "scripts/m4db_setup_database"
      ],
      classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "Topic :: Database",
            "License :: OSI Approved :: MIT License",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.6",
            "Programming Language :: Python :: 3.7",
            "Programming Language :: Python :: 3.8",
      ]),

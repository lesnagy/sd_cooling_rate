#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np

from argparse import ArgumentParser

from sd_cooling_rate.material_parameters import saturation_magnetization_func
from sd_cooling_rate.material_parameters import exchange_constant_func
from sd_cooling_rate.material_parameters import anisotropy_constant_func
from sd_cooling_rate.material_parameters import saturation_energy_func
from sd_cooling_rate.material_parameters import lambda_ex_func
from sd_cooling_rate.material_parameters import q_hardness_func


def get_command_line_parser():
    parser = ArgumentParser()

    parser.add_argument("material",
                        help="the material for which we wish to display the parameter graphs.")

    return parser


def main():
    parser = get_command_line_parser()
    args = parser.parse_args()

    fun_ms = saturation_magnetization_func(args.material)
    fun_aex = exchange_constant_func(args.material)
    fun_k1 = anisotropy_constant_func(args.material)
    fun_kd = saturation_energy_func(args.material)
    fun_lex = lambda_ex_func(args.material)
    fun_qhd = q_hardness_func(args.material)
    
    ms = list(map(lambda t: fun_ms(t), temperatures))
    a = list(map(lambda t: exchange_constant(t, material=material), temperatures))
    k1 = list(map(lambda t: anisotropy_constant(t, material=material), temperatures))
    kd = list(map(lambda t: saturation_energy(t, material=material), temperatures))
    lex = list(map(lambda t: lambda_ex(t, material=material), temperatures))
    qhd = list(map(lambda t: q_hardness(t, material=material), temperatures))

    plt.close('all')

    f, axis_array = plt.subplots(3, 2, sharex='col')

    axis_array[0][0].plot(temperatures, ms)
    axis_array[0][0].set_title('Ms')
    axis_array[0][0].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

    axis_array[1][0].plot(temperatures, a)
    axis_array[1][0].set_title('A')

    axis_array[2][0].plot(temperatures, k1)
    axis_array[2][0].set_title('K1')
    axis_array[2][0].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

    axis_array[0][1].plot(temperatures, kd)
    axis_array[0][1].set_title('Kd')
    axis_array[0][1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

    axis_array[1][1].plot(temperatures, lex)
    axis_array[1][1].set_title('Exchange length')

    axis_array[2][1].plot(temperatures, qhd)
    axis_array[2][1].set_title('q-hardness')

    plt.suptitle('Material parameters for {}'.format(program_options.material))
    plt.show()


if __name__ == '__main__':
    main()

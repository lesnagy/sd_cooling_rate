#!/usr/bin/env python

from math import *


def saturation_magnetization_func(material):
    r"""
    Returns a function that will compute the saturation magnetization for a given temperature.
    :param material: the material for which the function is required.
    :return: a function that will compute the saturation magnetization as a function of degrees Celsius.
    """
    if material == "iron":
        def func(t):
            t = 273.0 + t
            return 1.75221e6 - 1.21716e3 * t + 33.3368 * (t ** 2) - 0.363228 * (t ** 3) + 1.96713e-3 * (t ** 4) \
                   - 5.98015e-6 * (t ** 5) + 1.06587e-8 * (t ** 6) - 1.1048e-11 * (t ** 7) + 6.16143e-15 * (t ** 8) \
                   - 1.42904e-18 * (t ** 9)
        return func
    elif material == "magnetite":
        def func(t):
            t_c = 580.0
            return 737.384 * 51.876 * (t_c - t) ** 0.4
        return func
    else:
        raise ValueError("Requested saturation magnetization function for unsupported material '{}'".format(material))


def exchange_constant_func(material):
    r"""
    Returns a function that will compute the ferromagnetic exchange constant for a given temperature.
    :param material: the material for which the function is required.
    :return: a function that will compute the ferromagnetic exchange constant as a function of degrees Celsius.
    """
    if material == "iron":
        def func(t):
            t = 273.0 + t
            return -1.8952e-12 + 3.0657e-13 * t - 1.599e-15 * (t ** 2) + 4.0151e-18 * (t ** 3) - 5.3728e-21 * (t ** 4) \
                   + 3.6501e-24 * (t ** 5) - 9.9515e-28 * (t ** 6)
        return func
    elif material == "magnetite":
        def func(t):
            t_c = 580.0
            return (sqrt(21622.526 + 816.476 * (t_c - t)) - 147.046) / 408.238e11
        return func
    else:
        raise ValueError("Requested ferromagnetic exchange constant function for unsupported material '{}'".format(material))


def anisotropy_constant_func(material):
    r"""
    Returns a function that will compute the magnetocrystalline anisotropy constant for a given temperature.
    :param material: the material for which the function is required.
    :return: a function that will compute the magnetocrystalline anisotropy constant as a function of degrees Celsius.
    """
    if material == 'iron':
        def func(t):
            t = (273.0 + t)
            k1 = 54967.1 + 44.2946 * t - 0.426485 * (t ** 2) + 0.000811152 * (t ** 3) - 1.07579e-6 * (t ** 4) + \
                8.83207e-10 * (t ** 5) - 2.90947e-13 * (t ** 6)
            k1 = k1 * (480.0 / 456.0)
            return k1
        return func
    elif material == 'magnetite':
        def func(t):
            t_c = 580.0
            return -2.13074e-5 * (t_c - t) ** 3.2
        return func
    else:
        raise ValueError("Requested magnetocrystalline anisotropy constant function for unsupported material '{}'".format(material))


def saturation_energy_func(material):
    r"""
    Returns a function that will compute the saturation energy function.
    :param material: the material for which the function is required.
    :return: a function that will compute the saturation energy as a function of degrees Celsius.
    """
    def func(t):
        mu = 1e-7
        sat_mag = saturation_magnetization_func(material)
        ms = sat_mag(t)
        return 4 * 3.1415926535897932 * mu * ms * ms * 0.5
    return func


def lambda_ex_func(material):
    r"""
    Returns a function that will compute the lambda ex.
    :param material: the material for which the function is required.
    :return: a function that will compute the lambda ex as a function of degrees Celsius.
    """
    def func(t):
        aex = exchange_constant_func(material)
        kd = saturation_energy_func(material)

        try:
            r_val = sqrt(aex(t) / kd(t))
            return r_val
        except ValueError:
            return None
        except ZeroDivisionError:
            return None
    return func


def q_hardness_func(t, material):
    r"""
    Returns a function that will compute the q-hardness.
    :param material: the material for which the function is required.
    :return: a function that will compute the q-hardness as a function of degrees Celsius.
    """
    def func(t):
        k1 = anisotropy_constant_func(material)
        kd = saturation_energy_func(material)

        try:
            r_val = k1(t) / kd(t)
            return r_val
        except ValueError:
            return None
        except ZeroDivisionError:
            return None
    return func


def material_parameters(t, material):
    r"""
    returns the material parameters as a python dictionary.
    :param t: the temperature at which material parameters are required.
    :param material: the material for which the parameters are required.
    :return: material parameters as a python dictionary.
    """
    if material == 'iron':
        anisotropy_form = 'cubic'
    elif material == 'magnetite':
        anisotropy_form = 'cubic'
    else:
        raise ValueError("Attempting to calculate material parameters for unknown material '{}'".format(material))

    ms = saturation_magnetization_func(material)
    aex = exchange_constant_func(material)
    k1 = anisotropy_constant_func(material)
    kd = saturation_energy_func(material)
    lex = lambda_ex_func(material)
    qhd = q_hardness_func(material)

    return {'anisotropy_form': anisotropy_form,
            'material': material,
            'Aex': aex(t),
            'Ms': ms(t),
            'K1': k1(t),
            'Kd': kd(t),
            'lambda_ex': lex(t),
            'q_hardness': qhd(t)}


def iron_parameters(t):
    r"""
    Returns material parameters for iron for a given temperature.
    :param t: the temperature in degrees Celsius.
    :return: material parameters for iron at t degrees Celsius.
    """
    return material_parameters(t, 'iron')


def magnetite_parameters(t):
    r"""
    Returns material parameters for magnetite for a given temperature.
    :param t: the temperature in degrees Celsius.
    :return: material parameters for magnetite at t degrees Celsius.
    """
    return material_parameters(t, 'magnetite')

r"""
A collection of useful utility functions.
"""


def to_micron(x, unit):
    r"""
    Convert the input quantity x, to micron for a given unit.
    :param x: the quantity to convert to micron.
    :param unit: the unit to of the input quantity x.
    :return: the value of x in micron
    """
    if unit == "m":
        return x * 1E-6
    elif unit == "cm":
        return x * 1E-4
    elif unit == "mm":
        return x * 1E-3
    elif unit == "um":
        return x
    elif unit == "nm":
        return x * 1E3
    elif unit == "pm":
        return x * 1E6
    else:
        raise ValueError("Unsupported unit '{}'".format(unit))
